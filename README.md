**Valuer**<sup>([@npm](https://npmjs.org/@valuer/main))</sup> is an advanced declarative value validator.

***

## Installation:

```cmd
$ npm install @valuer/main --save
```

## Usage:

This code relies on a specific format of used values. But it will not fail with invalid ones:

```ts
function getRoot(radicand: number, degree: number): number {
    // roots of negative numbers do not make sense in JavaScript
    // therefore radicand should not be negative
    return radicand ** degree ** -1;
}

getRoot(8, 3);
// 2 (no error)

getRoot(-8, 3);
// NaN (no error, but should be)
```

To validate a value before its use, just preform validation a couple of lines before.

```ts
import { valuer } from "@valuer/main";

function getRoot(radicand: number, degree: number): number {
    valuer(radicand, "radicand").as({ spectrum: "non-negative" });

    return radicand ** degree ** -1;
}

getRoot(8, 3);
// 2 (no error)

getRoot(-8, 3);
// Validation failed: radicand is a negative number: value <number> -8
```

## Legacy notes:

To view versions prior to 0.10.3, visit https://npmjs.org/package/valur