import "mocha";
import { expect } from "chai";
import { valuer, re } from "./helpers";

describe('Using sole validations', () => {
	it("accepts known strings", () => {
		expect(() => valuer(17).as("negative", "integer")).to.throw(re.failed);
		expect(() => valuer(17).as("positive", "integer")).to.not.throw();
		expect(() => valuer(42).as("number")).to.not.throw();
	});

	it('accepts thunks', () => {
		expect(() => valuer(42).as(() => true)).to.not.throw();

		expect(() => valuer(42).as(() => false)).to.throw(/does not meet the constraint:/);
		expect(() => valuer(42).as(() => false, () => false)).to.throw(/does not meet the constraint \[0\]:/);
		expect(() => valuer(42).as(() => true, () => false)).to.throw(/does not meet the constraint \[1\]:/);
	});

	it('accepts regular expressions', () => {
		expect(() => valuer("text").as(/string/)).to.throw(re.failed);
		expect(() => valuer(42).as(/42/)).to.throw(re.failed);

		expect(() => valuer("text").as(/text/)).to.not.throw();
	});

	it('accepts lists of values', () => {
		expect(() => valuer(42).as([ 17 ])).to.throw(re.failed);
		expect(() => valuer(42).as([ 42 ])).to.not.throw();

		expect(() => valuer(42).as([ 13, 15 ])).to.throw(re.failed);
		expect(() => valuer(42).as([ 42, 17 ])).to.not.throw();

		expect(() => valuer("text").as([ "string" ])).to.throw(re.failed);
		expect(() => valuer("text").as([ "text" ])).to.not.throw();

		const composite: any = { prop: null };

		expect(() => valuer(composite).as([{ prop: null }])).to.throw(re.failed);
		expect(() => valuer(composite).as([ composite ])).to.not.throw();
	});

	it('throws on other values', () => {
		expect(() => valuer(42).as(<any> 17)).to.throw(/^Unknown validation: \(index 0\)/);
		expect(() => valuer(42).as([13], <any> 15)).to.throw(/^Unknown validation: \(index 1\)/);
		expect(() => valuer("text").as(<any> "text")).to.throw(/^Unknown validation: \(index 0\) \<string\> text/);
	});
});
