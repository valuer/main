import "mocha";
import { expect } from "chai";
import { valuer, re, Descriptor } from "./helpers";

const value = 42;
const valid: Descriptor = { typeOf: "number" };
const invalid: Descriptor = { typeOf: "string" };

describe('Basic functionality', () => {
	it('the "valuer(...).as(...)" syntax should work', () => {
		expect(() => valuer(value).as(valid)).to.not.throw();
		expect(() => valuer(value).as(invalid)).to.throw(re.failed);
	});

	it('the "valuer.as(...)(...)" syntax should work', () => {
		expect(() => valuer.as(valid)(value)).to.not.throw();
		expect(() => valuer.as(invalid)(value)).to.throw(re.failed);
	});
});
