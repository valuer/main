import "mocha";
import { expect } from "chai";
import { valuer, re } from "./helpers";

describe("Using persistent config", () => {
	const testCase = () => valuer(17).as("negative", "integer");

	before('ensure initials are as expected', () => {
		expect(testCase).to.throw(re.failed);
	});

	afterEach('reset config', () => {
		valuer.config({ mode: "assert", checkStrict: true, details: "value" });
	});

	// ***

	it('should change behavior for subsequent validations', () => {
		valuer.config({ mode: "check" });
		expect(testCase()).to.be.false;

		valuer.config({ checkStrict: false });
		expect(testCase()).to.be.true;
	});

	it('should give up priority to the explicitly provided config', () => {
		const value = 42;
		const descriptor = { value: 43 };
		const message = "value is not <number> 43";

		valuer.config({ details: "none" });

		expect(() => valuer(value).as(descriptor, { details: "value" })).to.throw(message);
		expect(() => valuer(value).as(descriptor)).to.not.throw(message);
	});
});
