import "mocha";
import { expect } from "chai";
import { valuer, re, inf } from "../helpers";

describe('Validator "range"', () => {
	it("[meta] should be a pair of numbers except NaN", () => {
		const valid = [ [0,1], [1,0], [0,inf], [inf,0], [-inf, inf] ];
		const invalid = [ [NaN,inf], [NaN,NaN], [0] ];

		for (const range of valid)
			expect(() => valuer.as({ range })).to.not.throw();

		for (const range of invalid)
			expect(() => valuer.as({ range })).to.throw(re.invalid);
	});

	it("should validate numbers within a particular range", () => {
		const range = [ 42, -17 ];

		for (const number of [ 42, -17, 0 ])
			expect(() => valuer(number).as({ range })).to.not.throw();

		for (const number of [ -100, Infinity, NaN ])
			expect(() => valuer(number).as({ range })).to.throw(re.failed);
	});
});
