import "mocha";
import { expect } from "chai";
import { valuer, re, Descriptor } from "../helpers";

describe('Validator "composite"', () => {
	it("[meta] should be a map of thunks or descriptors", () => {
		const maps: any[] = [ {}, { foo: { kind: "any" } }, { bar: { 'is invalid': () => false } } ];
		const junk: any[] = [ 42, null, [], [ "positive", "integer", 17 ], { bar: { kind: "idgas" } } ];

		for (const map of maps)
			expect(() => valuer.as({ composite: map })).to.not.throw();

		for (const obj of junk)
			expect(() => valuer.as({ composite: obj })).to.throw(re.invalid);
	});

	it('should validate composite values by their properties', () => {
		type Sex = "male" | "female";

		type Person = {
			age: number;
			sex: Sex;
			niceness: number;
		};

		// ***

		const age: Descriptor<number> = {
			kind: "primitive-non-void",
			typeOf: "number",
			number: "integer",
			spectrum: "positive",
			range: [ 0, 150 ],
		};

		const sex: Descriptor<Sex> = {
			set: [ "male", "female" ],
		};

		const niceness: Descriptor<number> = {
			kind: "primitive-non-void",
			typeOf: "number",
			range: [ 0, 1 ],
		};

		const person: Descriptor<Person> = {
			composite: { age, sex, niceness },
		};

		expect(() => valuer(42).as(person)).to.throw(/^Validation failed: value is not a composite value/);

		// ***

		valuer.define<number>(":age", age);

		const boy: Descriptor<Person> = {
			composite: {
				age: [ ":age", (v: number) => v < 18 ],
				sex: {
					value: "male",
				},
			},
		};

		expect(() => valuer({ age: 40, sex: "male" }).as(boy))
			.to.throw(/^Validation failed: 'age' in value does not meet/);

		expect(() => valuer({ age: 11, sex: "female" }).as(boy))
			.to.throw(/^Validation failed: 'sex' in value is not <string> male/);

		expect(() => valuer({ age: 13, sex: "male" }).as(boy)).to.not.throw();

		// ***

		const nice: Descriptor<Partial<Person>> = {
			composite: {
				niceness: {
					'is not very high': (v: number) => v > .5,
				},
			},
		};

		const hitler: Person = { age: 56, sex: "male", niceness: .1 };
		const gandhi: Person = { age: 78, sex: "male", niceness: .9 };

		valuer.define<Person>(":person", person);
		valuer.define<Partial<Person>>(":nice", nice);

		expect(() => valuer(hitler, "Adolf Hitler").as(":nice", ":person"))
			.to.throw(/^Validation failed: 'niceness' in Adolf Hitler is not very high/);

		expect(() => valuer(gandhi, "Mahatma Gandhi").as(":nice", ":person")).to.not.throw();
	});

	it('shows exactly the value that failed for composite validations', () => {
		const value = { prop: 42 };

		expect(valuer(value).as({ value })).to.equal(value);
		expect(() => valuer(value).as({ composite: { prop: { equals: 17 } } }))
			.to.throw(/^Validation failed: 'prop' in value is not equal to <number> 17: value <number> 42/);
	})
});
