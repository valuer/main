import "mocha";
import { expect } from "chai";
import { valuer, NumberSpectrum, re } from "../helpers";

const junkExceptNaN: any[] = [ { answer: 42 }, [{}], "text", (() => {}) ];
const junk: any[] = [ NaN, ...junkExceptNaN ];
const negatives: number[] = [ -17, -42 ];
const positives: number[] = [ 17, 42 ];
const zeroes: 0[] = [ -0, 0, +0 ];

describe('Validator "spectrum"', () => {
	it("[meta] should be a part of number spectrum", () => {
		const valid: NumberSpectrum[] = [ "any", "number", "positive", "negative", "non-positive", "non-negative", "non-zero" ];

		for (const spectrum of valid)
			expect(() => valuer.as({ spectrum })).to.not.throw();

		for (const spectrum of [ "", "integer", "finite" ])
			expect(() => valuer.as(<any> { spectrum })).to.throw(re.invalid);
	});

	// any
	it('should validate anything', () => {
		for (const object of [ ...junk, ...negatives, ...positives, ...zeroes ])
			expect(() => valuer(object).as({ spectrum: "any" })).to.not.throw();
	});

	// number
	it('should validate numbers', () => {
		for (const object of [ NaN, ...negatives, ...positives, ...zeroes ])
			expect(() => valuer(object).as({ spectrum: "number" })).to.not.throw();

		for (const object of [ ...junkExceptNaN ])
			expect(() => valuer(object).as({ spectrum: "number" })).to.throw(re.failed);
	});

	// non-zero
	it('should validate numbers except zero', () => {
		for (const object of [ NaN, ...negatives, ...positives ])
			expect(() => valuer(object).as({ spectrum: "non-zero" })).to.not.throw();

		for (const object of [ ...junkExceptNaN, ...zeroes ])
			expect(() => valuer(object).as({ spectrum: "non-zero" })).to.throw(re.failed);
	});

	// positive
	it('should validate positive numbers', () => {
		for (const object of [ ...positives ])
			expect(() => valuer(object).as({ spectrum: "positive" })).to.not.throw();

		for (const object of [ ...junk, ...negatives, ...zeroes ])
			expect(() => valuer(object).as({ spectrum: "positive" })).to.throw(re.failed);
	});

	// negative
	it('should validate negative numbers', () => {
		for (const object of [ ...negatives ])
			expect(() => valuer(object).as({ spectrum: "negative" })).to.not.throw();

		for (const object of [ ...junk, ...positives, ...zeroes ])
			expect(() => valuer(object).as({ spectrum: "negative" })).to.throw(re.failed);
	});

	// non-positive
	it('should validate non-positive numbers', () => {
		for (const object of [ ...negatives, ...zeroes ])
			expect(() => valuer(object).as({ spectrum: "non-positive" })).to.not.throw();

		for (const object of [ ...junk, ...positives ])
			expect(() => valuer(object).as({ spectrum: "non-positive" })).to.throw(re.failed);
	});

	// non-negative
	it('should validate non-negative numbers', () => {
		for (const object of [ ...positives, ...zeroes ])
			expect(() => valuer(object).as({ spectrum: "non-negative" })).to.not.throw();

		for (const object of [ ...junk, ...negatives ])
			expect(() => valuer(object).as({ spectrum: "non-negative" })).to.throw(re.failed);
	});
});
