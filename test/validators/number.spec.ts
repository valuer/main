import "mocha";
import { expect } from "chai";
import { valuer, Descriptor, NumberKind, re } from "../helpers";

describe('Validator "number"', () => {
	it("[meta] should be a valid class of numbers", () => {
		const legacy: any[] = [ "float only", "odd", "even" ];
		const invalid: any[] = [ "", "evan", "positive", "integer-saf" ];
		const valid: NumberKind[] = [
			"any", "number", "numeric", "finite", "non-integer", "non-integer-finite",
			"integer", "integer-even", "integer-odd",
			"integer-safe", "integer-even-safe", "integer-odd-safe",
		];

		for (const number of valid)
			expect(() => valuer.as({ number })).to.not.throw();

		for (const number of [ ...invalid, ...legacy ])
			expect(() => valuer.as({ number })).to.throw(re.invalid);
	});

	// any
	it('"any" should validate anything', () => {
		const descriptor: Descriptor = { number: "any" };

		for (const object of [ 17, 42, NaN, Infinity, [], {}, "text", /regex/ ])
			expect(() => valuer(object).as(descriptor)).to.not.throw();
	});

	// number
	it('"number" should validate numbers', () => {
		const descriptor: Descriptor = { number: "number" };

		for (const object of [ 17, 42, NaN, Infinity ])
			expect(() => valuer(object).as(descriptor)).to.not.throw();

		for (const object of [ [], {}, "text", /regex/ ])
			expect(() => valuer(object).as(descriptor)).to.throw(re.failed);
	});

	// numeric
	it('"numeric" should validate any number, except NaN', () => {
		const descriptor: Descriptor = { number: "numeric" };

		for (const object of [ 17, 42, Infinity ])
			expect(() => valuer(object).as(descriptor)).to.not.throw();

		for (const object of [ [], {}, NaN, "text", /regex/ ])
			expect(() => valuer(object).as(descriptor)).to.throw(re.failed);
	});

	// finite
	it('"finite" should validate finite numbers', () => {
		const descriptor: Descriptor = { number: "finite" };

		for (const object of [ 17, 42 ])
			expect(() => valuer(object).as(descriptor)).to.not.throw();

		for (const object of [ [], {}, Infinity, NaN, "text", /regex/ ])
			expect(() => valuer(object).as(descriptor)).to.throw(re.failed);
	});

	// non-integer
	it('"non-integer" should validate non-integer finite numbers', () => {
		const descriptor: Descriptor = { number: "non-integer" };

		for (const object of [ 17.42, 15.5, Math.PI, Infinity, NaN ])
			expect(() => valuer(object).as(descriptor)).to.not.throw();

		for (const object of [ 17, 42, 1.0, [], {}, "text", /regex/ ])
			expect(() => valuer(object).as(descriptor)).to.throw(re.failed);
	});

	// non-integer-finite
	it('"non-integer-finite" should validate non-integer finite numbers', () => {
		const descriptor: Descriptor = { number: "non-integer-finite" };

		for (const object of [ 17.42, 15.5, Math.PI ])
			expect(() => valuer(object).as(descriptor)).to.not.throw();

		for (const object of [ 17, 42, 1.0, Infinity, NaN, [], {}, "text", /regex/ ])
			expect(() => valuer(object).as(descriptor)).to.throw(re.failed);
	});

	// integer
	it('"integer" should validate integers', () => {
		const descriptor: Descriptor = { number: "integer" };

		for (const object of [ 17, 42, 1.0, Number.MAX_SAFE_INTEGER ])
			expect(() => valuer(object).as(descriptor)).to.not.throw();

		for (const object of [ 17.42, -Math.PI, Infinity, NaN, [], {}, "text", /regex/ ])
			expect(() => valuer(object).as(descriptor)).to.throw(re.failed);
	});

	// integer-even
	it('"integer-even" should validate even integers', () => {
		const descriptor: Descriptor = { number: "integer-even" };

		for (const object of [ 42, 2, -1118 ])
			expect(() => valuer(object).as(descriptor)).to.not.throw();

		for (const object of [ 17, 1.0, -Math.PI, Infinity, NaN, [], {}, "text", /regex/ ])
			expect(() => valuer(object).as(descriptor)).to.throw(re.failed);
	});

	// integer-odd
	it('"integer-odd" should validate odd integers', () => {
		const descriptor: Descriptor = { number: "integer-odd" };

		for (const object of [ 17, 1.0 ])
			expect(() => valuer(object).as(descriptor)).to.not.throw();

		for (const object of [ 42, 2, -1118, -Math.PI, Infinity, NaN, [], {}, "text", /regex/ ])
			expect(() => valuer(object).as(descriptor)).to.throw(re.failed);
	});

	// integer-safe
	it('"integer-safe" should validate safe integers', () => {
		const descriptor: Descriptor = { number: "integer-safe" };

		for (const object of [ 17, 1.0, 42, 2, -1118, Number.MAX_SAFE_INTEGER, Number.MIN_SAFE_INTEGER ])
			expect(() => valuer(object).as(descriptor)).to.not.throw();

		for (const object of [ Number.MAX_SAFE_INTEGER + 1, Number.MIN_SAFE_INTEGER - 1, Infinity, NaN, [], {}, "text", /regex/ ])
			expect(() => valuer(object).as(descriptor)).to.throw(re.failed);
	});

	// integer-even-safe
	it('"integer-even-safe" should validate even safe integers', () => {
		const descriptor: Descriptor = { number: "integer-even-safe" };

		for (const object of [ 42, 2, -1118, Number.MAX_SAFE_INTEGER - 1, Number.MIN_SAFE_INTEGER + 1 ])
			expect(() => valuer(object).as(descriptor)).to.not.throw();

		for (const object of [ Number.MAX_SAFE_INTEGER, Number.MIN_SAFE_INTEGER, Infinity, NaN, [], {}, "text", /regex/ ])
			expect(() => valuer(object).as(descriptor)).to.throw(re.failed);
	});

	// integer-odd-safe
	it('"integer-odd-safe" should validate odd safe integers', () => {
		const descriptor: Descriptor = { number: "integer-odd-safe" };

		for (const object of [ 17, 1.0, Number.MAX_SAFE_INTEGER, Number.MIN_SAFE_INTEGER ])
			expect(() => valuer(object).as(descriptor)).to.not.throw();

		for (const object of [ Infinity, NaN, [], {}, "text", /regex/ ])
			expect(() => valuer(object).as(descriptor)).to.throw(re.failed);
	});
});
