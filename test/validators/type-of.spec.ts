import "mocha";
import { expect } from "chai";
import { TypeOf } from "@valuer/types";
import { valuer, junk, re, nonVoidPrims, undefs } from "../helpers";

describe('Validator "typeOf"', () => {
	it("[meta] should be a result of `typeof`", () => {
		const valid: TypeOf[] = [ "string", "number", "boolean", "symbol", "undefined", "object", "function" ];

		for (const typeOf of valid)
			expect(() => valuer.as({ typeOf })).to.not.throw();

		for (const typeOf of junk)
			expect(() => valuer.as({ typeOf })).to.throw(re.invalid);
	});

	it("should validate values by result of applying `typeof` to them", () => {
		const values = [
			...nonVoidPrims,
			...undefs,
			{}, [], <object> null,
			(() => {}),
		];

		for (const value of values)
			expect(() => valuer(value).as({ typeOf: typeof value })).to.not.throw();

		for (const prim of nonVoidPrims)
			expect(() => valuer<object>(Object(prim)).as({ typeOf: typeof prim })).to.throw(re.failed);
	});
});
