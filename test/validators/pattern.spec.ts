import "mocha";
import { expect } from "chai";
import { valuer, re } from "../helpers";

describe('Validator "pattern"', () => {
	it("[meta] should be an instance of RegExp", () => {
		const valid = [ /regex/, /hello/g, new RegExp(".*", "m") ];
		const invalid: any[] = [ "/regex/", 42 ];

		for (const pattern of valid)
			expect(() => valuer.as({ pattern })).to.not.throw();

		for (const pattern of invalid)
			expect(() => valuer.as({ pattern })).to.throw(re.invalid);
	});

	it("should validate strings, that match the pattern", () => {
		const descriptor = { pattern: /\d{3}-\d{4}/ };

		expect(() => valuer("555-0123").as(descriptor)).to.not.throw();
		expect(() => valuer(null).as(descriptor)).to.throw(re.failed);

		for (const string of [ "Bruce", "Almighty" ])
			expect(() => valuer(string).as(descriptor)).to.throw(re.failed);
	});
});
