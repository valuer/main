import "mocha";
import { expect } from "chai";
import { valuer, re } from "../helpers";

describe('Validator "length"', () => {
	it("[meta] should be a natural number", () => {
		const floats = Array(10).fill(0).map(() => Math.random() * (42 - 17) + 17);
		const naturals = floats.map(Math.round);

		for (const valid of naturals)
			expect(() => valuer.as({ length: valid })).to.not.throw();

		for (const invalid of floats)
			expect(() => valuer.as({ length: invalid })).to.throw(re.invalid);
	});

	it("should validate a length of strings", () => {
		expect(() => valuer("1234567890").as({ length: 10 })).to.not.throw();
		expect(() => valuer("hello").as({ length: 10 })).to.throw(re.failed);
	});

	it("should validate a length of arrays", () => {
		expect(() => valuer([ 1, 2, 3, 4, 5 ]).as({ length: 5 })).to.not.throw();
		expect(() => valuer([ 1 ]).as({ length: 5 })).to.throw(re.failed);
	});
});
