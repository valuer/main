import "mocha";
import { expect } from "chai";
import { valuer, Descriptor, ValueKind, junk, re, nonVoidPrims, nonVoidObjs, nils, undefs } from "../helpers";

describe('Validator "kind"', () => {
	it("[meta] should be a valid description of a value kind", () => {
		const kinds: ValueKind[] = [
			"any",
			"primitive", "composite", "void",
			"primitive-non-void", "composite-non-void", "non-void",
			"primitive-defined", "composite-defined", "defined",
			"primitive-non-null", "composite-non-null", "non-null",
		];

		const legacy: any[] = [
			"object", "not-null-object", "non-null-object", "non-null-composite", "non-void-object", "non-void-composite", "defined-object", "defined-composite", "not-null-primitive", "non-null-primitive", "non-void-primitive", "defined-primitive", "not-null",
		];

		for (const kind of kinds)
			expect(() => valuer.as({ kind })).to.not.throw();

		for (const kind of [ ...junk, ...legacy ])
			expect(() => valuer.as({ kind })).to.throw(re.invalid);
	});

	// any
	it('"any" should validate anything', () => {
		const descriptor: Descriptor = { kind: "any" };

		for (const value of [ ...nonVoidPrims, ...nonVoidObjs, ...nils, ...undefs ])
			expect(() => valuer(value).as(descriptor)).to.not.throw();
	});

	// void
	it('"void" should validate voids', () => {
		const descriptor: Descriptor = { kind: "void" };

		for (const value of [ ...nils, ...undefs ])
			expect(() => valuer(value).as(descriptor)).to.not.throw();

		for (const value of [ ...nonVoidPrims, ...nonVoidObjs ])
			expect(() => valuer(value).as(descriptor)).to.throw(re.failed);
	});

	// non-null
	it('"non-null" should validate anything except `null`', () => {
		const descriptor: Descriptor = { kind: "non-null" };

		for (const value of [ ...undefs, ...nonVoidPrims, ...nonVoidObjs ])
			expect(() => valuer(value).as(descriptor)).to.not.throw();

		for (const value of [ ...nils ])
			expect(() => valuer(value).as(descriptor)).to.throw(re.failed);
	});

	// non-void
	it('"non-void" should validate non-void values', () => {
		const descriptor: Descriptor = { kind: "non-void" };

		for (const value of [ ...nonVoidPrims, ...nonVoidObjs ])
			expect(() => valuer(value).as(descriptor)).to.not.throw();

		for (const value of [ ...nils, ...undefs ])
			expect(() => valuer(value).as(descriptor)).to.throw(re.failed);
	});

	// primitive
	it('"primitive" should validate primitive values', () => {
		const descriptor: Descriptor = { kind: "primitive" };

		for (const value of [ ...nils, ...undefs, ...nonVoidPrims ])
			expect(() => valuer(value).as(descriptor)).to.not.throw();

		for (const value of [ ...nonVoidObjs ])
			expect(() => valuer(value).as(descriptor)).to.throw(re.failed);
	});

	// primitive-defined
	it('"primitive-defined" should validate all primitive values except `undefined`', () => {
		const descriptor: Descriptor = { kind: "primitive-defined" };

		for (const value of [ ...nils, ...nonVoidPrims ])
			expect(() => valuer(value).as(descriptor)).to.not.throw();

		for (const value of [ ...undefs, ...nonVoidObjs ])
			expect(() => valuer(value).as(descriptor)).to.throw(re.failed);
	});

	// primitive-non-null
	it('"primitive-non-null" should validate all primitive values except `null`', () => {
		const descriptor: Descriptor = { kind: "primitive-non-null" };

		for (const value of [ ...undefs, ...nonVoidPrims ])
			expect(() => valuer(value).as(descriptor)).to.not.throw();

		for (const value of [ ...nils, ...nonVoidObjs ])
			expect(() => valuer(value).as(descriptor)).to.throw(re.failed);
	});

	// primitive-non-void
	it('"primitive-non-void" should validate non-void primitive values', () => {
		const descriptor: Descriptor = { kind: "primitive-non-void" };

		for (const value of [ ...nonVoidPrims ])
			expect(() => valuer(value).as(descriptor)).to.not.throw();

		for (const value of [ ...nils, ...undefs, ...nonVoidObjs ])
			expect(() => valuer(value).as(descriptor)).to.throw(re.failed);
	});

	// composite
	it('"composite" should validate composite values', () => {
		const descriptor: Descriptor = { kind: "composite" };

		for (const value of [ ...nils, ...nonVoidObjs ])
			expect(() => valuer(value).as(descriptor)).to.not.throw();

		for (const value of [ ...undefs, ...nonVoidPrims ])
			expect(() => valuer(value).as(descriptor)).to.throw(re.failed);
	});

	// composite-defined
	it('"composite-defined" should validate all composite values or `null`, but not `undefined`', () => {
		const descriptor: Descriptor = { kind: "composite-defined" };

		for (const value of [ ...nils, ...nonVoidObjs ])
			expect(() => valuer(value).as(descriptor)).to.not.throw();

		for (const value of [ ...undefs, ...nonVoidPrims ])
			expect(() => valuer(value).as(descriptor)).to.throw(re.failed);
	});

	// composite-non-null
	it('"composite-non-null" should validate all composite values except `null`', () => {
		const descriptor: Descriptor = { kind: "composite-non-null" };

		for (const value of [ ...undefs, ...nonVoidObjs ])
			expect(() => valuer(value).as(descriptor)).to.not.throw();

		for (const value of [ ...nils, ...nonVoidPrims ])
			expect(() => valuer(value).as(descriptor)).to.throw(re.failed);
	});

	// composite-non-void
	it('"composite-non-void" should validate non-void composites', () => {
		const descriptor: Descriptor = { kind: "composite-non-void" };

		for (const value of [ ...nonVoidObjs ])
			expect(() => valuer(value).as(descriptor)).to.not.throw();

		for (const value of [ ...nonVoidPrims, ...nils, ...undefs ])
			expect(() => valuer(value).as(descriptor)).to.throw(re.failed);
	});

	// defined
	it('"defined" should validate anything except `undefined`', () => {
		const descriptor: Descriptor = { kind: "defined" };

		for (const value of [ ...nils, ...nonVoidPrims, ...nonVoidObjs ])
			expect(() => valuer(value).as(descriptor)).to.not.throw();

		for (const value of [ ...undefs ])
			expect(() => valuer(value).as(descriptor)).to.throw(re.failed);
	});
});
