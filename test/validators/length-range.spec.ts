import "mocha";
import { expect } from "chai";
import { valuer, re, inf } from "../helpers";

describe('Validator "lengthRange"', () => {
	it("[meta] should be a pair of natural numbers", () => {
		const valid = [ [42,17], [17,42], [0,42] ];
		const invalid: any[] = [ [NaN,inf], [NaN,NaN], [-1,15] ];

		for (const lengthRange of valid)
			expect(() => valuer.as({ lengthRange })).to.not.throw();

		for (const lengthRange of invalid)
			expect(() => valuer.as({ lengthRange })).to.throw(re.invalid);
	});

	it("[meta] might be a range with unlimited upper bound (only)", () => {
		expect(() => valuer.as({ lengthRange: [ 4, inf ] })).to.not.throw();
		expect(() => valuer.as({ lengthRange: [ inf, 4 ] })).to.not.throw();
		expect(() => valuer.as({ lengthRange: [ inf, inf ] })).to.throw(re.invalid);
	});

	const lengthRange = [ 0, 10 ];

	it("should validate a length range of strings", () => {
		const short = "hi";
		const long = "Lorem ipsum dolor sit amet, consectetur adipiscing elit";

		expect(() => valuer(short).as({ lengthRange })).to.not.throw();
		expect(() => valuer(long).as({ lengthRange })).to.throw(re.failed);
	});

	it("should validate a length range of arrays", () => {
		const short = [ 1, 2, 3, 4, 5 ];
		const long = Array(1e3).fill(0);

		expect(() => valuer(short).as({ lengthRange })).to.not.throw();
		expect(() => valuer(long).as({ lengthRange })).to.throw(re.failed);
	});
});
