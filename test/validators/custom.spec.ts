import "mocha";
import { expect } from "chai";
import { valuer, re } from "../helpers";

describe('Validator "custom"', () => {
	it("[meta] should be thunks", () => {
		const junk: any[] = [ 42, "text", /regex/, { "is number": () => false }, { "is number": false }, { "is": 17 }, [] ];
		const valid = [ () => true, () => false ];

		for (const custom of valid)
			expect(() => valuer.as({ custom })).to.not.throw();

		for (const custom of junk)
			expect(() => valuer.as({ custom })).to.throw(re.invalid);
	});

	it("[meta] can have custom failure messages", () => {
		const messages = [ "is invalid", "is not positive", "has no 'name' property" ];

		for (const message of messages)
			expect(() => valuer.as({ [message]: () => true })).to.not.throw();
	});

	it("should apply custom constraint to values", () => {
		type Guardian = { name: string; };
		type Avenger = { hero: string; };
		type Person = Guardian | Avenger;

		const stark: Avenger = { hero: "Iron Man" };
		const steve: Avenger = { hero: "Captain America" };
		const groot: Guardian = { name: "Groot" };

		valuer.define(":avenger", {
			"is not an avenger": (person: Person) => "hero" in person,
		});

		valuer.define(":ironMan", {
			"is not an Iron Man": (avenger: Avenger) => avenger.hero === "Iron Man",
		});

		valuer.define(":captain", {
			"is not a Captain America": (avenger: Avenger) => avenger.hero === "Captain America",
		});

		expect(() => valuer(stark, "Stark").as(":avenger", ":captain")).to.throw(/^Validation failed: Stark is not a Captain America/);
		expect(() => valuer(stark, "Stark").as(":avenger", ":ironMan")).to.not.throw();

		expect(() => valuer(steve, "Steve").as(":avenger", ":ironMan")).to.throw(/^Validation failed: Steve is not an Iron Man/);
		expect(() => valuer(steve, "Steve").as(":avenger", ":captain")).to.not.throw();

		expect(() => valuer(groot, "Groot").as(":avenger")).to.throw(/^Validation failed: Groot is not an avenger/);
	});
});
