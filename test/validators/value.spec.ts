import "mocha";
import { expect } from "chai";
import { valuer, junk, re } from "../helpers";

describe('Validator "value"', () => {
	it("[meta] can be anything", () => {
		for (const value of junk)
			expect(() => valuer.as({ value })).to.not.throw();
	});

	it("should validate particular value", () => {
		for (const value of junk)
			expect(() => valuer(value).as({ value })).to.not.throw();

		expect(() => valuer(17).as({ value: 42 })).to.throw(re.failed);
		expect(() => valuer({}).as({ value: {} })).to.throw(re.failed);
	});
});
