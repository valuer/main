import "mocha";
import { expect } from "chai";
import { valuer, re } from "../helpers";

describe('Validator "set"', () => {
	it("[meta] should be an arbitrary array", function() {
		const valid = [ [], Array(), Array.from(Object()), "text".match(/\w/g) ];
		const junk: any[] = [ 42, "text", /regex/, {}, (() => {}), new Set, arguments ];

		for (const set of valid)
			expect(() => valuer.as({ set })).to.not.throw();

		for (const set of junk)
			expect(() => valuer.as({ set })).to.throw(re.invalid);
	});

	it("should validate particular set of values", () => {
		const values: any[] = [ 42, 17, 3.5, null, NaN, {} ];
		const validate = valuer.as({ set: values });

		expect(() => validate(4_2)).to.not.throw();
		expect(() => validate(NaN)).to.not.throw();
		expect(() => validate({ })).to.throw(re.failed);
	});
});
