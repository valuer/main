import "mocha";
import { expect } from "chai";
import { valuer, re, junk } from "../helpers";

describe('Validator "instanceOf"', () => {
	it("[meta] should be a class", () => {
		const classes = [ Object, String, Number, Array, RegExp ];
		const _junk: any[] = [ 42, "text", /regex/, {}, [] ];

		for (const instanceOf of classes)
			expect(() => valuer.as({ instanceOf })).to.not.throw();

		for (const instanceOf of _junk)
			expect(() => valuer.as({ instanceOf })).to.throw(re.invalid);
	});

	it("[meta] cannot be a plain function"); // FIXME:

	it("should validate instances of a particular class", () => {
		const Thingy = class {};
		const thingy = new Thingy();

		expect(() => valuer(thingy).as({ instanceOf: Thingy })).to.not.throw();

		for (const object of junk)
			expect(() => valuer(object).as({ instanceOf: Thingy })).to.throw(re.failed);
	});
});
