import "mocha";
import { expect } from "chai";
import { valuer, junk, re } from "../helpers";

describe('Validator "equal"', () => {
	it("[meta] can be anything", () => {
		for (const value of junk)
			expect(() => valuer.as({ equals: value })).to.not.throw();
	});

	it("should validate strictly equal values", () => {
		for (const value of junk)
			expect(() => valuer(value).as({ equals: value })).to.not.throw();
	});

	it("should validate primitive-composite pair of alike values", () => {
		const alike = [
			{
				value: 42,
				equals: [ 42, Object(42) ],
				notEquals: [ "42", Object("42") ]
			},
			{
				value: "hello",
				equals: [ "hello", Object("hello") ],
				notEquals: [ "hi", Object("hi") ]
			},
		];

		for (const { value, equals, notEquals } of alike) {
			for (const operand of equals)
				expect(() => valuer(value).as({ equals: operand })).to.not.throw();

			for (const operand of notEquals)
				expect(() => valuer(value).as({ equals: operand })).to.throw(re.failed);
		}
	});

	it('should validate objects with the same structure', () => {
		expect(() => valuer({}).as({ equals: {} })).to.not.throw();
		expect(() => valuer({ prop: 17 }).as({ equals: { prop: 17 } })).to.not.throw();
		expect(() => valuer({ }).as({ equals: { prop: 17 } })).to.throw(re.failed);
	});

	it("should validate recursive objects with similar structure", () => {
		const object: any = {};
		const same: any = {};

		object.itself = object;
		same.itself = same;

		expect(() => valuer(object).as({ equals: same })).to.not.throw();

		const different: any = { differs: true };
		different.itself = different;

		expect(() => valuer(object).as({ equals: different })).to.throw(re.failed);
	});
});
