export { valuer } from "../src";
export * from "../src/types";

export const re = {
	failed: /^Validation failed:/,
	invalid: /^Invalid validator:/,
};

export const inf = Infinity;
export const junk: any[] = [ 42, "text", /regex/, {}, [], (() => {}) ];
export const nonVoidPrims = [ 42, 17, "text", NaN, inf, true, Symbol.species ];
export const nonVoidObjs: object[] = [ {}, (() => {}), /regex/ ];
export const nils = [ null, "a".match(/b/) as null ];
export const undefs: void[] = [ undefined, void 0 ];
