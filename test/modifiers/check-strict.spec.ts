import "mocha";
import { expect } from "chai";
import { valuer, Descriptor, junk } from "../helpers";

const value = -15;
const descriptor: Descriptor = { spectrum: "positive", number: "integer" };

describe('Modifier "checkStrict"', () => {
	it("[meta] should be a boolean", () => {
		for (const checkStrict of [ true, false ])
			expect(() => valuer.as(descriptor, { checkStrict })).to.not.throw();

		for (const checkStrict of junk)
			expect(() => valuer.as(descriptor, { checkStrict })).to.throw(/^Invalid config:/);
	});

	it("should validate values with at least one validation successfuly passed", () => {
		expect(valuer(value).as(descriptor, { mode: "check", checkStrict: false })).to.be.true;
	});

	it("should force all validation to be successfuly passed", () => {
		expect(valuer(value).as(descriptor, { mode: "check", checkStrict: true })).to.be.false;
	});
});
