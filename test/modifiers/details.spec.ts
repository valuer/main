import "mocha";
import { expect } from "chai";
import { valuer, Descriptor, Modification, junk } from "../helpers";

const descriptor: Descriptor = { typeOf: "string" };

describe('Modifier "details"', () => {
	it("[meta] should be a valid string", () => {
		const options: Modification<"details">[] = [ "none", "message", "value", "validator", "validation", "all" ];

		for (const details of options)
			expect(() => valuer.as(descriptor, { details })).to.not.throw();

		for (const details of junk)
			expect(() => valuer.as(descriptor, { details })).to.throw(/^Invalid config:/);
	});

	it('should change detalization of "Validation failed" error message', () => {
		expect(() => valuer(17).as(descriptor, { details: "none" }))
			.to.throw(/^Validation failed$/);

		expect(() => valuer(17).as(descriptor, { details: "message" }))
			.to.throw(/^Validation failed: value is not of type \"string\"$/);

		expect(() => valuer(17).as(descriptor, { details: "value" }))
			.to.throw(/^Validation failed: value is not of type \"string\": value \<number\> 17$/);
	});
});