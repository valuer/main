import "mocha";
import { expect } from "chai";
import { valuer, Config } from "../helpers";

/** @private */
const using = (config: Config) => valuer.config(config);

describe('Modifier "rangeInclusiveness"', () => {
	after(() => {
		using({ rangeInclusiveness: [ true, true ] });
	});

	// ***

	it("[meta] has to be a pair of booleans", () => {
		const boundaries = [true, false];
		const invalid = [
			[true, true, true],
			[false],
			[]
		];

		for (const a of boundaries)
			for (const b of boundaries)
				expect(() => using({ rangeInclusiveness: [a, b] })).to.not.throw();

		for (const rangeInclusiveness of invalid)
			expect(() => using({ rangeInclusiveness } as any)).to.throw("not a pair of boolean values");
	});

	const range = [ -1, 1 ];

	it('should make both range boundaries inclusive', () => {
		const validate = valuer.as<number>({ range }, { rangeInclusiveness: [ true, true ] });

		expect(() => validate(-1)).to.not.throw();
		expect(() => validate(1)).to.not.throw();
	});

	it('should make only upper boundary inclusive', () => {
		const validate = valuer.as<number>({ range }, { rangeInclusiveness: [ false, true ] });

		expect(() => validate(-1)).to.throw('out of bounds "(-1 to 1]"');
		expect(() => validate(1)).to.not.throw();
	});

	it('should make only lower boundary inclusive', () => {
		const validate = valuer.as<number>({ range }, { rangeInclusiveness: [ true, false ] });

		expect(() => validate(-1)).to.not.throw();
		expect(() => validate(1)).to.throw('out of bounds "[-1 to 1)"');
	});

	it('should make both range boundaries exclusive', () => {
		const validate = valuer.as<number>({ range }, { rangeInclusiveness: [ false, false ] });

		expect(() => validate(-1)).to.throw('out of bounds "(-1 to 1)"');
		expect(() => validate(1)).to.throw('out of bounds "(-1 to 1)"');
	});
});
