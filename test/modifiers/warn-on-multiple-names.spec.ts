import "mocha";
import { expect } from "chai";
import { valuer } from "../helpers";

describe('Modifier "warnOnMultipleNames"', () => {
	const original = console.warn;
	const invokations: any[][] = [];

	before('monkey-patch the "console.warn"', () => {
		console.warn = (...args: any[]) => invokations.push(args);
	});

	after('restore the original "console.warn"', () => {
		console.warn = original;
	});

	afterEach('restore default config', () => {
		valuer.config({ warnOnMultipleNames: false });
	});

	// ***

	it('show show warning if definition already exists', () => {
		valuer.config({ warnOnMultipleNames: true });

		valuer.define(":positive-1st", { spectrum: "positive" });
		valuer.define(":positive-2nd", [ "positive" ]);

		expect(invokations).to.deep.equal([[ `Descriptor has been defined as ":positive-1st" and ":positive-2nd"` ]]);
	});
});
