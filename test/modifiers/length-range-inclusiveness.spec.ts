import "mocha";
import { expect } from "chai";
import { valuer, Config } from "../helpers";

/** @private */
const using = (config: Config) => valuer.config(config);

describe('Modifier "lengthRangeInclusiveness"', () => {
	after(() => {
		using({ lengthRangeInclusiveness: [ true, true ] });
	});

	// ***

	it("[meta] has to be a pair of booleans", () => {
		const boundaries = [true, false];
		const invalid = [
			[true, true, true],
			[false],
			[]
		];

		for (const a of boundaries)
			for (const b of boundaries)
				expect(() => using({ lengthRangeInclusiveness: [a, b] })).to.not.throw();

		for (const lengthRangeInclusiveness of invalid)
			expect(() => using({ lengthRangeInclusiveness } as any)).to.throw("not a pair of boolean values");
	});

	const lengthRange = [ 1, 4 ];

	it('should make both range boundaries inclusive', () => {
		const validate = valuer.as<string>({ lengthRange }, { lengthRangeInclusiveness: [ true, true ] });

		expect(() => validate("1")).to.not.throw();
		expect(() => validate("1234")).to.not.throw();
	});

	it('should make only upper boundary inclusive', () => {
		const validate = valuer.as<string>({ lengthRange }, { lengthRangeInclusiveness: [ false, true ] });

		expect(() => validate("1")).to.throw('not (1 to 4] elements long');
		expect(() => validate("1234")).to.not.throw();
	});

	it('should make only lower boundary inclusive', () => {
		const validate = valuer.as<string>({ lengthRange }, { lengthRangeInclusiveness: [ true, false ] });

		expect(() => validate("1")).to.not.throw();
		expect(() => validate("1234")).to.throw('not [1 to 4) elements long');
	});

	it('should make both range boundaries exclusive', () => {
		const validate = valuer.as<string>({ lengthRange }, { lengthRangeInclusiveness: [ false, false ] });

		expect(() => validate("1")).to.throw('not (1 to 4) elements long');
		expect(() => validate("1234")).to.throw('not (1 to 4) elements long');
	});
});
