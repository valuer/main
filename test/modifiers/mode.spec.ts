import "mocha";
import { expect } from "chai";
import { valuer, Descriptor, Modification, re, junk } from "../helpers";

const value = 17;
const valid: Descriptor = { typeOf: typeof value };
const invalid: Descriptor = { typeOf: "string" };

describe('Modifier "mode"', () => {
	it('[meta] has to be a valid string', () => {
		const modes: Modification<"mode">[] = [ "assert", "check", "describe", "switch" ];

		for (const mode of modes)
			expect(() => valuer.as(valid, { mode })).to.not.throw();

		for (const mode of junk)
			expect(() => valuer.as(valid, { mode })).to.throw(/^Invalid config:/);
	});

	it('"assert" throws error on failure', () => {
		const mode = "assert";

		expect(valuer(value).as(valid, { mode })).to.equal(value);
		expect(() => valuer(value).as(invalid, { mode })).to.throw(re.failed);
	});

	it('"check" returns `false` on failure', () => {
		const mode = "check";

		expect(valuer(value).as(valid, { mode })).to.be.true;
		expect(valuer(value).as(invalid, { mode })).to.be.false;
	});

	it('"describe" returns validation results', () => {
		const mode = "describe";

		expect(valuer(value).as(valid, { mode })).to.deep.equal({ typeOf: null });
		expect(valuer(value).as(invalid, { mode })).to.deep.equal({ typeOf: 'value is not of type "string"' });
	});

	it('"switch" returns failure information', () => {
		const mode = "switch";
		const props = { name: 'typeOf', value };
		const long = 'Validation failed: value is not of type "string": value <number> 17';
		const short = "Validation failed";

		expect(valuer(value).as(valid, { mode })).to.deep.equal({});
		expect(valuer(value).as(invalid, { mode })).to.deep.equal({ ...props, message: long });
		expect(valuer(value).as(invalid, { mode, details: "none" })).to.deep.equal({ ...props, message: short });
	});
});
