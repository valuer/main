import "mocha";
import { expect } from "chai";
import { valuer, re } from "./helpers";

describe("Using named descriptors", () => {
	it('should disallow empty descriptor names', () => {
		for (const empty of [ "", ":" ])
			expect(() => valuer.define(empty, {})).to.throw(/descriptor name is empty/);
	});

	it('should disallow non-string descriptor names', () => {
		for (const invalid of [ 16, null ])
			expect(() => valuer.define(<any> invalid, {})).to.throw(/descriptor name is not a string/);

		expect(() => valuer.define("valid", {})).to.not.throw();
	});

	it('should define descriptor using descriptor itself', () => {
		expect(() => valuer.define("natural", {
			kind: "primitive-non-void",
			typeOf: "number",
			number: "integer",
			spectrum: "non-negative",
		})).to.not.throw();
	});

	it("should define descriptor using list of sole validations", () => {
		expect(() => valuer.define("sixteen", [[16]])).to.not.throw();
	});

	it('should throw on invalid descriptor definition', () => {
		const junk: any[] = [ null, 42, [42], /whatever/ ];

		for (const value of junk)
			expect(() => valuer.define("whatever", value)).to.throw();
	});

	it('should disallow overriding descriptors', () => {
		expect(() => valuer.define("sixteen", [[17]])).to.throw(/cannot redefine/);
	});

	it("should validate a value by descriptor's name(s)", () => {
		valuer.define(":gmail", {
			kind: "primitive-non-void",
			pattern: /^\w+@gmail.com$/,
		});

		valuer.define(":dashed", [ /-/g ]);

		expect(() => valuer("parzhitsky@fail.ru").as(":gmail")).to.throw(re.failed);
		expect(() => valuer("parzhitsky@gmail.com").as("string", ":gmail")).to.not.throw();
		expect(() => valuer("parzhitsky@gmail.com").as(":gmail", ":dashed")).to.throw(re.failed);
	});

	it("should throw error if descriptor is not defined", () => {
		expect(() => valuer(42).as(":unknown")).to.throw(/descriptor ":unknown" is not defined/);
	});
});
