/** @internal Default failure for custom validation */
export const DEFAULT_FAILURE = "does not meet the constraint";

/** @internal Failure for unknown string */
export const UNKNOWN_STRING_FAILURE = "is unknown string";
