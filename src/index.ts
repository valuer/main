import { Role } from "./types/role";

import { Configs, Config } from "./modules/configs";
import { Descriptors, Descriptor } from "./modules/descriptors";
import { Hooks, Result } from "./modules/hooks";
import { Processors } from "./modules/processors";
import { ValidationSole } from "./modules/validators";

// ***

type ResultFactoryDirect<Value = any> = {
	/**
	 * @example
	 * const kind = "primitive";
	 * const mode = "check";
	 *
	 * valuer.as({ kind }, { mode })(42, "The Answer")
	 * valuer.as(kind)(42, "The Answer")
	 */
	(value: Value, role?: Role): Result<Value>
};

type ResultFactoryReverse<Value = any> = {
	/**
	 * @example
	 * const kind = "primitive";
	 * const mode = "check";
	 *
	 * valuer(42, "The Answer").as({ kind }, { mode })
	 */
	(descriptor: Descriptor<Value>, config?: Config): Result<Value>;

	/**
	 * @example
	 * valuer(42, "The Answer").as("primitive")
	 */
	(...validations: ValidationSole<Value>[]): Result<Value>;
};

// ***

/**
 * @example
 * const kind = "primitive";
 * const mode = "check";
 *
 * valuer.as({ kind }, { mode })(42, "The Answer")
 */
function core<Value = any>(descriptor: Descriptor<Value>, config?: Config): ResultFactoryDirect<Value>;

/**
 * @example
 * valuer.as("primitive")(42, "The Answer")
 */
function core<Value = any>(...validations: ValidationSole<Value>[]): ResultFactoryDirect<Value>;

function core<Value = any>(...args: any[]) {
	const usingDescriptor = Descriptors.isDescriptorFlow(args);

	const config = Configs.create(usingDescriptor? args[1] : {});
	const hooks = Hooks.create<Value>(config);
	const processors = Processors.create(args, usingDescriptor, config);

	return (value: Value, role: Role = "value"): Result<Value> => {
		Configs.setupCurrent(config);

		for (const processor of processors) {
			const processing = Processors.apply(processor, value);

			if (processing.result)
				hooks["validation.success"](processing);

			else hooks["validation.failure"](processing, role);

			if (hooks["validation.break"]())
				break;
		}

		Configs.clearCurrent();

		return hooks["validation.return"](value);
	};
}

// ***

/** @private */
type Delegator = <Value>(value: Value, role?: Role) => {
	as: ResultFactoryReverse<Value>;
};

/** @private */
const delegator: Delegator = (value, role?) => ({
	as: (...args: any[]) =>
		core(...args)(value, role),
});

export const valuer = Object.assign(delegator, {
	as: core,
	config: Configs.changeDefault,
	define: Descriptors.define,
});
