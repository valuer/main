export { Role } from "./role";
export { Failure } from "./failure";

export {
	ConfigBase,
	Config,
	Modifier,
	ModifierString,
	ModifierNonString,
	ModifierFailure,
	Modification,
} from "../modules/configs";

export {
	ValueKind,
	NumberKind,
	NumberSpectrum,
	DescriptorName,
	DescriptorBase,
	Descriptor,
	Descriptive,
} from "../modules/descriptors";

export {
	ErrorKind,
	ErrorMessage,
} from "../modules/errors";

export {
	Validator,
	ValidatorWithoutDefaultValidation,
	ValidatorWithDefaultValidation,
	ValidatorString,
	ValidatorStringCustom,
	ValidatorNonString,
	ValidatorFailure,
	Validation,
	ValidationString,
	ValidationNonString,
	ValidationSole,
} from "../modules/validators";
