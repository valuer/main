import { Thunk } from "@valuer/types";
import { is } from "@valuer/is";

/** @internal */
export function isThunk<Value = any>(value: any): value is Thunk<Value> {
	return is._function(value) /* additional verifications are probably required */;
}
