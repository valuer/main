import { Thunk, NumericRange } from "@valuer/types";
import { help } from "@valuer/help";
import { is } from "@valuer/is";
import { fn } from "@valuer/fn";

import { UNKNOWN_STRING_FAILURE } from "../constants";
import { isThunk } from "../helpers";

import { Errors } from "./errors";
import { DescriptorBase, DescriptorName, ValueKind, NumberKind, NumberSpectrum } from "./descriptors";

// ***

export type Validator = keyof DescriptorBase;
export type ValidatorWithoutDefaultValidation = Validator & (
	"value" | "equals" | "set" | "typeOf" | "instanceOf" | "length" | "composite"
);
export type ValidatorWithDefaultValidation = Exclude<Validator, ValidatorWithoutDefaultValidation>;
export type ValidatorString = Validator & ("typeOf" | "number" | "spectrum" | "kind");
export type ValidatorStringCustom = Exclude<ValidatorString, "typeOf">;
export type ValidatorNonString = Exclude<Validator, ValidatorString>;
export type ValidatorFailure = string;

export type Validation<
	Value = any,
	V extends Validator = Validator,
> = DescriptorBase<Value>[V];

export type ValidationString<
	V extends ValidatorString = ValidatorString,
> = DescriptorBase[V];

export type ValidationNonString<
	V extends ValidatorNonString = ValidatorNonString,
> = DescriptorBase[V];

export type ValidationSole<
	Value = any,
> = DescriptorName | Thunk<Value> | ValidationString | Validation<Value, "pattern" | "set">;

// ***

/** @private */
const VALIDATOR_TO_DEFAULT_VALIDATION_MAP: Readonly<{
	[V in ValidatorWithDefaultValidation]: NonNullable<Validation<any, V>>;
} & {
	[V in ValidatorWithoutDefaultValidation]: undefined;
}> = Object.freeze({
	value: undefined,
	equals: undefined,
	set: undefined,
	kind: <ValueKind> "any",
	composite: undefined,

	typeOf: undefined,
	instanceOf: undefined,

	number: <NumberKind> "any",
	spectrum: <NumberSpectrum> "any",
	range: <NumericRange> Object.freeze([-Infinity, Infinity]),

	pattern: /(?:)/,
	length: undefined,
	lengthRange: <NumericRange> Object.freeze([0, Infinity]),
});

/** @private */
const VALIDATOR_STRING_TO_VALIDATION_STRING_LIST_MAP: {
	[V in ValidatorString]: ValidationString<V>[];
} = {
	kind: [
		"any", "composite", "primitive", "void",
		"primitive-non-void", "composite-non-void", "non-void",
		"primitive-defined", "composite-defined", "defined",
		"primitive-non-null", "composite-non-null", "non-null",
	],

	typeOf: [
		"string", "number", "boolean", "symbol", "undefined", "object", "function",
	],

	number: [
		"any", "number", "numeric", "finite", "non-integer", "non-integer-finite",
		"integer", "integer-even", "integer-odd",
		"integer-safe", "integer-even-safe", "integer-odd-safe",
	],

	spectrum: [
		"any", "number", "positive", "negative", "non-positive", "non-negative", "non-zero",
	],
};

/** @private */
const VALIDATOR_TO_META_VALIDATOR_THUNK_MAP: {
	[V in Validator]: Thunk<Validation<any, V>>;
} = {
	...help.convertMapFlat(VALIDATOR_STRING_TO_VALIDATION_STRING_LIST_MAP, is.inArray),

	value: is._any,
	equals: is._any,
	set: is.array<any>(),
	composite: fn.all(is.directInstanceOf(Object), (object: object) => Object.values(object).every(is._nonVoidComposite)),
	instanceOf: is._function,
	range: is.array(2, is.numeric()),
	pattern: is.instanceOf(RegExp),
	length: is.natural(),
	lengthRange: is.lengthRange(),
};

/** @private */
const VALIDATOR_TO_VALIDATOR_FAILURE_MAP: {
	[V in Validator]: ValidatorFailure;
} = {
	...help.convertMapFlat(VALIDATOR_STRING_TO_VALIDATION_STRING_LIST_MAP, () => UNKNOWN_STRING_FAILURE),

	value: null, // no constraints
	equals: null, // no constraints
	set: "is not an array",
	composite: "is not a map of descriptors",
	instanceOf: "is not a class",
	range: "is invalid range",
	pattern: "is not a regular expression",
	length: "is not a natural number",
	lengthRange: "is invalid length range",
};

// ***

/** @private */
const isValidatorString = (validator: string): validator is ValidatorString => {
	return validator in VALIDATOR_STRING_TO_VALIDATION_STRING_LIST_MAP;
};

// ***

/** @internal */
export const Validators = {
	default: VALIDATOR_TO_DEFAULT_VALIDATION_MAP,

	// ***

	isValidationString<V extends ValidatorString>(validation: string, validator?: V): validation is ValidationString<V> {
		const map = VALIDATOR_STRING_TO_VALIDATION_STRING_LIST_MAP;

		if (validator != null)
			return is.inArray(map[validator])(validation);

		else for (const _validator in map)
			if (Validators.isValidationString(validation, _validator as V))
				return true;

		return false;
	},

	isValidator(validator: string): validator is Validator {
		return validator in VALIDATOR_TO_DEFAULT_VALIDATION_MAP;
	},

	getFor(validation: ValidationString, index: number): ValidatorString[] {
		const map = VALIDATOR_STRING_TO_VALIDATION_STRING_LIST_MAP;
		const validators: ValidatorString[] = [];

		for (const validator in map)
			if (is._any<ValidatorString>(validator))
				if (Validators.isValidationString(validation, validator))
					validators.push(validator);

		if (!validators.length)
			return Errors.unknown.validation(validation, index);

		else return validators;
	},

	validate<V extends Validator>(validator: V, validation: Validation<any, V>) {
		const thunk: Thunk<Validation<any, V>> = VALIDATOR_TO_META_VALIDATOR_THUNK_MAP[validator];

		const invalid = () => Errors.panic(
			"Invalid validator",
			`"${ validator }" ${ VALIDATOR_TO_VALIDATOR_FAILURE_MAP[validator] }`,
			`validation ${ help.getPrintable(validation) }`,
		);

		if (isValidatorString(validator)) {
			if (!Validators.isValidationString(validation, validator))
				return invalid();
		}

		else if (!thunk(validation))
			return invalid();
	},

	validateThunk(validation: Validation): typeof validation {
		if (isThunk(validation))
			return validation;

		else return Errors.panic(
			"Invalid validator",
			`"(custom)" is not a thunk`,
			`validation ${ help.getPrintable(validation) }`,
		);
	},
};
