import { is } from "@valuer/is";
import { help } from "@valuer/help";

import { Failure } from "../types/failure";

import { Configs, Modification } from "./configs";

// ***

export type ErrorKind =
	| "Invalid validator"
	| "Invalid config"
	| "Invalid descriptor"
	| "Unknown validation"
	| "Validation failed"
	| "Unexpected error";

export type ErrorMessage = string;

export type PanicArgs = [ ErrorKind, Failure, ...string[] ];

// ***

/** @private */
const ERROR_MESSAGE_PARTS_HEAD_LENGTH = 2;

/** @private */
const SEPARATOR_NAME_TO_SEPARATOR_MAP = {
	head: ": " as ": ",
	tail: ", " as ", ",
};

/** @private */
const DETAILS_MODIFICATION_TO_DETALIZATION_LEVEL_MAP: {
	[M in Modification<"details">]: number;
} = {
	"all": 0,
	"none": 1,
	"message": 2,
	"value": 3,
	"validator": 4,
	"validation": 5,
};

/** @private */
const ERROR_KIND_MAP: Record<ErrorKind, true> = {
	"Invalid validator": true,
	"Invalid config": true,
	"Invalid descriptor": true,
	"Unknown validation": true,
	"Validation failed": true,
	"Unexpected error": true,
};

// ***

/** @private */
const reduceDetails = (kind: ErrorKind, text: string, ...info: string[]): PanicArgs => {
	if (!(kind in ERROR_KIND_MAP))
		return Errors.panic("Unexpected error", "invalid error kind", help.getPrintable(kind));

	else if (kind === "Validation failed") {
		const detalization = DETAILS_MODIFICATION_TO_DETALIZATION_LEVEL_MAP[Configs.current.details];

		if (detalization == null)
			return <never> Configs.validateModifier("details", Configs.current.details);

		if (detalization < ERROR_MESSAGE_PARTS_HEAD_LENGTH)
			text = null;

		if (detalization >= ERROR_MESSAGE_PARTS_HEAD_LENGTH - 1)
			info.length = Math.max(0, detalization - ERROR_MESSAGE_PARTS_HEAD_LENGTH);
	}

	return [ kind, text, ...info ];
};

// ***

/** @internal */
export const Errors = {
	composeMessage(kind: ErrorKind, text: string, ...info: string[]): ErrorMessage {
		const [ _kind, _text = null, ..._info ] = reduceDetails(kind, text, ...info);

		const separators = SEPARATOR_NAME_TO_SEPARATOR_MAP;

		const head = [ _kind, _text ].filter(is._nonVoid).join(separators.head);
		const tail = _info.filter(is._nonVoid).join(separators.tail);
		const glue = tail && separators.head;

		return head + glue + tail;
	},

	panic(kind: ErrorKind, text: string, ...info: string[]): never {
		const message = Errors.composeMessage(kind, text, ...info);

		if (kind === "Validation failed")
			Configs.clearCurrent();

		throw new Error(message);
	},

	unknown: {
		validation: (validation: any, index: number) => Errors.panic(
			"Unknown validation",
			`(index ${ index }) ${ help.getPrintable(validation) }`,
		),

		modifier: (modifier: any) => Errors.panic(
			"Invalid config",
			`modifier "${ modifier }" is unknown`,
		),
	},
};
