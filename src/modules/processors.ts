import { Thunk } from "@valuer/types";
import { brackets } from "@valuer/brackets";
import { help } from "@valuer/help";
import { is } from "@valuer/is";
import { fn } from "@valuer/fn";

import { DEFAULT_FAILURE } from "../constants";
import { Failure, Role } from "../types";

import { ConfigBase } from "./configs";
import { Descriptors, Descriptor } from "./descriptors";
import { Errors, ErrorMessage, PanicArgs } from "./errors";
import { Validators, Validator, ValidatorString, ValidatorNonString, ValidatorStringCustom, Validation, ValidationString, ValidationSole } from "./validators";

// ***

/** @internal */
export type Processor = {
	key: Validator | Failure;
	path: string[];
	thunk: Thunk;
	failure: Failure;
	validator: Validator | "(custom)";
	validation: Validation;
};

/** @internal */
export type Processing = {
	value: any;
	result: boolean;
	processor: Processor;
};

/** @private */
type FactoryConfigured<
	Input = any,
	Output = any,
> = {
	(input: Input, config: ConfigBase): Output;
};

// ***

/** @private */
const DEFAULT_PROP_SEPARATOR = ".";

/** @private */
const DEFAULT_NAME_SEPARATOR = "@";

/** @private */
const PROPS_PREPOSITION = " in ";

// ***

/** @private */
const VALIDATOR_STRING_CUSTOM_TO_VALIDATION_STRING_TO_FAILURE_MAP_MAP: {
	[V in ValidatorStringCustom]: {
		[Vn in ValidationString<V>]: Failure;
	};
} = {
	kind: {
		"any": null, // no failure possible

		"void": "is not a void",
		"defined": "is undefined",
		"non-null": "is null",
		"non-void": "is a void",

		"primitive": "is not a primitive",
		"primitive-defined": "is not a defined primitive",
		"primitive-non-null": "should be a primitive and not null",
		"primitive-non-void": "should be a primitive and not a void",

		"composite": "is not a composite",
		"composite-defined": "is not a defined composite",
		"composite-non-null": "should be a composite and not null",
		"composite-non-void": "should be a composite and not a void",
	},

	number: {
		"any": null, // no failure possible

		"number": "is not a number",
		"numeric": "is NaN",
		"finite": "is not a finite number",

		"non-integer": "should not be an integer",
		"non-integer-finite": "should be a finite non-integer number",

		"integer": "is not an integer",
		"integer-even": "is not an even integer",
		"integer-odd": "is not an odd integer",

		"integer-safe": "is not a safe integer",
		"integer-even-safe": "is not a safe even integer",
		"integer-odd-safe": "is not a safe odd integer",
	},

	spectrum: {
		"any": null, // no failure possible

		"number": "is not a number",
		"negative": "is not a negative number",
		"positive": "is not a positive number",
		"non-positive": "should not be a positive number",
		"non-negative": "should not be a negative number",
		"non-zero": "should not be zero",
	},
};

/** @private */
const VALIDATOR_TO_FAILURE_FACTORY_MAP: {
	[V in Validator]: FactoryConfigured<Validation<any, V>, Failure>;
} = {
	value: (value, config?) =>
		`is not ${ help.getPrintable(value) }`,

	equals: (equals, config?) =>
		`is not equal to ${ help.getPrintable(equals) }`,

	set: (set?, config?) =>
		`was not found in the set`,

	kind: (kind, config?) =>
		VALIDATOR_STRING_CUSTOM_TO_VALIDATION_STRING_TO_FAILURE_MAP_MAP.kind[kind],

	composite: (map?, config?) =>
		`is not a composite value`,

	// ***

	typeOf: (type, config?) =>
		`is not of type "${ type }"`,

	instanceOf: (klass, config?) =>
		`is not an instance of ${ klass.name }`,

	// ***

	number: (number, config?) =>
		VALIDATOR_STRING_CUSTOM_TO_VALIDATION_STRING_TO_FAILURE_MAP_MAP.number[number],

	spectrum: (spectrum, config?) =>
		VALIDATOR_STRING_CUSTOM_TO_VALIDATION_STRING_TO_FAILURE_MAP_MAP.spectrum[spectrum],

	range: (range, { rangeInclusiveness }) =>
		`is out of bounds "${ help.getPrintableRange(range, rangeInclusiveness) }"`,

	// ***

	pattern: (pattern, config?) =>
		`does not match the pattern "${ pattern }"`,

	length: (length, config?) =>
		`is not ${ length } element${ length === 1? '' : 's' } long`,

	lengthRange: (lengthRange, { lengthRangeInclusiveness }) =>
		`is not ${ help.getPrintableRange(lengthRange, lengthRangeInclusiveness) } elements long`,
};

/** @private */
const VALIDATOR_STRING_CUSTOM_TO_VALIDATION_STRING_TO_THUNK_MAP_MAP: {
	[V in ValidatorStringCustom]: {
		[Vn in ValidationString<V>]: Thunk;
	};
} = {
	kind: {
		"any": is._any,

		"void": is._void,
		"defined": fn.not(is._undefined),
		"non-null": fn.not(is._null),
		"non-void": is._nonVoid,

		"primitive": is._primitive,
		"primitive-defined": fn.any(is._nonVoidPrimitive, is._null),
		"primitive-non-null": fn.any(is._nonVoidPrimitive, is._undefined),
		"primitive-non-void": is._nonVoidPrimitive,

		"composite": is._composite,
		"composite-defined": fn.any(is._nonVoidComposite, is._null),
		"composite-non-null": fn.any(is._nonVoidComposite, is._undefined),
		"composite-non-void": is._nonVoidComposite,
	},

	number: {
		"any": is._any,

		"number": is._number,
		"numeric": fn.all(is._number, is.numeric()),
		"finite": fn.all(is._number, Number.isFinite),

		"non-integer": fn.all(is._number, is.float()),
		"non-integer-finite": fn.all(is._number, Number.isFinite, is.float()),

		"integer": Number.isInteger,
		"integer-even": is.integer("even"),
		"integer-odd": is.integer("odd"),

		"integer-safe": Number.isSafeInteger,
		"integer-even-safe": fn.all(Number.isSafeInteger, is.integer("even")),
		"integer-odd-safe": fn.all(Number.isSafeInteger, is.integer("odd")),
	},

	spectrum: {
		"any": is._any,

		"number": is._number,
		"negative": fn.all(is.numeric(), value => value < 0),
		"positive": fn.all(is.numeric(), value => value > 0),
		"non-positive": fn.all(is.numeric(), value => value <= 0),
		"non-negative": fn.all(is.numeric(), value => value >= 0),
		"non-zero": fn.all(is._number, value => value !== 0),
	},
};

/** @private */
const _isStringOrArray = fn.any(is._string, is.array());

/** @private */
const VALIDATOR_TO_THUNK_FACTORY_MAP: {
	[V in ValidatorString]: FactoryConfigured<ValidationString<V>, Thunk>;
} & {
	[V in ValidatorNonString]: FactoryConfigured<Validation<any, V>, Thunk>;
} = {
	value: (expected, config?) => (actual) =>
		actual === expected,

	equals: (expected, config?) => (actual) =>
		is.equalTo(expected)(actual),

	set: (array, config?) => (item) =>
		is.inArray(array)(item),

	kind: (kind, config?) =>
		VALIDATOR_STRING_CUSTOM_TO_VALIDATION_STRING_TO_THUNK_MAP_MAP.kind[kind],

	composite: (map?, config?) => (object) =>
		is._composite(object),

	// ***

	typeOf: (type, config?) => (value) =>
		is.ofType(type)(value),

	instanceOf: (klass, config?) => (instance) =>
		is.instanceOf(klass)(instance),

	// ***

	number: (number, config?) =>
		VALIDATOR_STRING_CUSTOM_TO_VALIDATION_STRING_TO_THUNK_MAP_MAP.number[number],

	spectrum: (spectrum, config?) =>
		VALIDATOR_STRING_CUSTOM_TO_VALIDATION_STRING_TO_THUNK_MAP_MAP.spectrum[spectrum],

	range: (bounds, { rangeInclusiveness }) => (value) =>
		is.inRange(bounds, rangeInclusiveness)(value),

	// ***

	pattern: (pattern, config?) =>
		fn.all(is._string, pattern.test.bind(pattern)),

	length: (length, config?) => (value) =>
		_isStringOrArray(value) && value.length === length,

	lengthRange: (lengthRange, { lengthRangeInclusiveness }) => (value) =>
		_isStringOrArray(value) && is.inRange(lengthRange, lengthRangeInclusiveness)(value.length),
};

// ***

/** @private */
const getThunk = <Value, V extends Validator>(validator: V, validation: Validation<Value, V>, config: ConfigBase): Thunk<Value> => {
	const thunkFactory: FactoryConfigured<Validation, Thunk> = VALIDATOR_TO_THUNK_FACTORY_MAP[validator];

	return thunkFactory(validation, config);
};

/** @private */
const getFailure = (validator: string, validation: Validation, config: ConfigBase): Failure => {
	if (validator === "custom")
		return DEFAULT_FAILURE;

	else if (!Validators.isValidator(validator))
		return String(validator) as Failure;

	return VALIDATOR_TO_FAILURE_FACTORY_MAP[validator](validation, config);
};

/** @private */
const forComposite = <Value extends object>(
	validation: Validation<Value, "composite">,
	config: ConfigBase,
	path: string[] = [],
): Processor[] => {
	const processors: Processor[] = [{
		key: "composite",
		path: [ ...path ],
		thunk: getThunk("composite", null, config),
		failure: getFailure("composite", null, config),
		validator: "composite",
		validation: is._composite,
	}];

	for (const key in validation) {
		const descriptor = Descriptors.create<Value>(validation[key]);

		processors.push(...Processors.create([ descriptor ], true, config, [ ...path, key ]));
	}

	return processors;
};

/** @private */
const pathToProps = (path: string[]) => {
	if (!path.length)
		return "";

	const _in_ = PROPS_PREPOSITION;

	return path.reverse().map(brackets.surroundBy(brackets.collection.singleQuotes.tight)).join(_in_) + _in_;
};

// ***

/** @internal */
export const Processors = {
	getKey({ path, key }: Processor): string {
		if (!path.length)
			return key;

		else return DEFAULT_PROP_SEPARATOR + path.join(DEFAULT_PROP_SEPARATOR) + DEFAULT_NAME_SEPARATOR + key;
	},

	getMessageTextPart(processor: Processor, role: Role): string {
		return `${ pathToProps(processor.path) + role } ${ processor.failure }`;
	},

	getPanicArgs(processor: Processor, value: any, role: Role): PanicArgs {
		return [
			"Validation failed",
			Processors.getMessageTextPart(processor, role),
			`value ${ help.getPrintable(value) }`,
			`validator ${ processor.validator }`,
			`validation ${ help.getPrintable(processor.validation) }`,
		];
	},

	getMessage(processor: Processor, value: any, role: Role): ErrorMessage {
		return Errors.composeMessage(...Processors.getPanicArgs(processor, value, role));
	},

	create(args: any[], usingDescriptor: boolean, config: ConfigBase, path: string[] = []) {
		const descriptive = usingDescriptor? args[0] as Descriptor : args as ValidationSole[];
		const descriptor = Descriptors.create(descriptive);
		const processors: Processor[] = [];

		for (const [ validator, validation ] of help.getEntries(descriptor)) {
			const _path = [ ...path ];

			if (validator === "composite") {
				processors.push(...forComposite(validation, config, _path));

				continue;
			}

			const mixin: Pick<Processor, "validator" | "thunk"> = Validators.isValidator(validator)? {
				validator,
				thunk: getThunk(validator, validation, config),
			} : {
				validator: "(custom)",
				thunk: validation,
			};

			processors.push({
				...mixin,
				key: validator,
				path: _path,
				failure: getFailure(validator, validation, config),
				validation,
			});
		}

		return processors;
	},

	apply(processor: Processor, source: any): Processing {
		const value = help.get(source)(processor.path);
		const result = processor.thunk(value);

		return { value, result, processor };
	},
};
