import { Thunk, TypeOf, Class } from "@valuer/types";
import { brackets } from "@valuer/brackets";
import { is } from "@valuer/is";
import { help } from "@valuer/help";
import * as deepmerge from "deepmerge";

import { DEFAULT_FAILURE } from "../constants";
import { isThunk } from "../helpers";

import { Errors } from "./errors";
import { Validators, ValidationSole, ValidationString } from "./validators";
import { Configs } from "./configs";

// ***

export type ValueKind =
	| "any"
	| "void" | "non-void" | "defined" | "non-null"
	| "primitive" | "primitive-non-void" | "primitive-defined" | "primitive-non-null"
	| "composite" | "composite-non-void" | "composite-defined" | "composite-non-null";

export type NumberKind =
	| "any"
	| "number"
	| "numeric"
	| "finite"
	| "non-integer" | "non-integer-finite"
	| "integer" | "integer-even" | "integer-odd"
	| "integer-safe" | "integer-even-safe" | "integer-odd-safe";

export type NumberSpectrum = "any" | "number" | "positive" | "negative" | "non-positive" | "non-negative" | "non-zero";
export type DescriptorName = string;

export type DescriptorBase<
	Value = any,
> = {
	/** Expected value */
	value: Value;

	/** Equal value */
	equals: Value;

	/** Set of allowed values */
	set: Value[];

	/** Kind of value */
	kind: ValueKind;

	/** Properties of composite value */
	composite: Value extends object ? { [Key in keyof Value]?: Descriptive<Value[Key]> } : never;

	// ***

	/** Expected result of applying `typeof` to the value */
	typeOf: TypeOf;

	/** Expected value's constructor */
	instanceOf: Value extends object ? Class<Value> : never;

	// ***

	/** Type of numeric value */
	number: NumberKind;

	/** Allowed part of the number spectrum */
	spectrum: NumberSpectrum;

	/** The smallest and the largest possible numeric values respectively */
	range: number[];

	// ***

	/** Pattern to match against the string */
	pattern: RegExp;

	/** Length of the string */
	length: number;

	/** Min and max length of the string respectively */
	lengthRange: number[];
};

export type Descriptor<
	Value = any,
> = Partial<DescriptorBase<Value>> & {
	[failure: string]: any;
};

export type Descriptive<
	Value = any,
> = Descriptor<Value> | ValidationSole<Value>[];


// ***

/** @private */
const DESCRIPTOR_NAME_PREFIX = ":";

// ***

/** @private */
const DESCRIPTOR_NAME_TO_DESCRIPTOR_MAP: Record<DescriptorName, Descriptor> = Object.create(null);

// ***

/** @private */
const validateName = (name: string): DescriptorName => {
	const invalid = (failure: string, ...args: string[]): never =>
		Errors.panic("Unexpected error", `descriptor name ${ failure }`, ...args);

	if (!is._string(name))
		return invalid("is not a string", help.getPrintable(name));

	// remove heading colon if present
	const trimmed = name.trim();
	const _name = trimmed.substr(+trimmed.startsWith(DESCRIPTOR_NAME_PREFIX));

	if (!_name.length)
		return invalid("is empty");

	else return DESCRIPTOR_NAME_PREFIX + _name;
};

/** @private */
const getNamesOf = <Value>(descriptive: Descriptive<Value>): DescriptorName[] => {
	const isSame = is.equalTo(Descriptors.create(descriptive));

	return Object.keys(Descriptors.defined).filter((name) => isSame(Descriptors.defined[name]));
};

/** @private */
const warnSimilar = (descriptor: Descriptor) => {
	const existing = getNamesOf(descriptor);

	if (existing.length <= 1)
		return;

	const names = existing.map(brackets.surroundBy(brackets.collection.doubleQuotes.tight));

	console.warn(`Descriptor has been defined as ${ help.getPrintableList(names) }`);
};

/** @private */
const getDefined = (name: DescriptorName): Descriptor => {
	if (name in Descriptors.defined)
		return Descriptors.defined[name];

	else return Errors.panic("Unexpected error", `descriptor "${ name }" is not defined`);
};

/** @private */
const addToSet = <Value>(descriptor: Descriptor<Value>, values: Value[]) => {
	if (!(descriptor.set instanceof Array))
		descriptor.set = [];

	descriptor.set.push(...values);
};

/** @private */
const createFromValidations = <Value>(validations: ValidationSole<Value>[]): Descriptor<Value> => {
	const descriptor: Descriptor<Value> = {};
	const thunks: Thunk<Value>[] = [];

	for (const [ index, validation ] of help.getEntries(validations))
		if (is._string(validation))
			if (Validators.isValidationString(validation))
				for (const validator of Validators.getFor(validation, index))
					(descriptor[validator] as ValidationString) = validation;

			else if (validation.startsWith(DESCRIPTOR_NAME_PREFIX))
				Object.assign(descriptor, deepmerge(descriptor, getDefined(validation)));

			else return Errors.unknown.validation(validation, index);

		else if (validation instanceof RegExp)
			descriptor.pattern = validation;

		else if (isThunk<Value>(validation))
			thunks.push(validation);

		else if (validation instanceof Array)
			addToSet(descriptor, validation);

		else return Errors.unknown.validation(validation, index);

	if (thunks.length)
		if (thunks.length > 1)
			for (const [ index, thunk ] of help.getEntries(thunks))
				descriptor[DEFAULT_FAILURE + ' ' + brackets.surround(index, brackets.collection.square.tight)] = thunk;

		else descriptor[DEFAULT_FAILURE] = thunks[0];

	return descriptor;
};

/** @private */
const disclose = <D extends Descriptor>(descriptor: D): typeof descriptor => {
	if ("composite" in descriptor)
		for (const key in descriptor.composite) {
			if (!Descriptors.isDescriptorFlow([ descriptor.composite[key] ]))
				descriptor.composite[key] = createFromValidations(descriptor.composite[key] as ValidationSole[]);

			descriptor.composite[key] = disclose(descriptor.composite[key]);
		}

	return descriptor;
};

// ***

/** @internal */
export const Descriptors = {
	get defined() {
		return DESCRIPTOR_NAME_TO_DESCRIPTOR_MAP;
	},

	// ***

	isDescriptorFlow(args: any[]): boolean {
		return is.inRange([ 1, 2 ])(args.length) && args.every(is.directInstanceOf(Object));
	},

	validate<Value>(descriptor: Descriptor<Value>): typeof descriptor {
		if (!is._objectNonNull(descriptor))
			return Errors.panic("Invalid descriptor", help.getPrintable(descriptor));

		else if (!is._any<Descriptor<Value>>(descriptor))
			<never> null;

		for (const [ validator, validation ] of help.getEntries(descriptor))
			if (Validators.isValidator(validator))
				Validators.validate(validator, validation);

			else Validators.validateThunk(validation);

		return descriptor;
	},

	create<Value>(descriptive: Descriptive<Value>): Descriptor<Value> {
		if (is.array()(descriptive))
			return createFromValidations(descriptive);

		else if (Descriptors.isDescriptorFlow([ descriptive ]))
			return disclose(Descriptors.validate(descriptive));

		else return Errors.panic("Invalid descriptor", help.getPrintable(descriptive));
	},

	/**
	 * @example
	 * const number = "integer";
	 * const spectrum = "positive";
	 *
	 * valuer.define(":natural", { number, spectrum });
	 * valuer.define(":natural", [ number, spectrum ]);
	 */
	define<Value>(name: DescriptorName, descriptive: Descriptive<Value>) {
		const _name = validateName(name);

		if (_name in Descriptors.defined)
			return Errors.panic("Unexpected error", `cannot redefine descriptor "${ _name }"`);

		else Descriptors.defined[_name] = Descriptors.create(descriptive);

		if (Configs.current.warnOnMultipleNames)
			warnSimilar(Descriptors.defined[_name]);
	},
};
