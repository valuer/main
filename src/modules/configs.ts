import { Thunk } from "@valuer/types";
import { help } from "@valuer/help";
import { is } from "@valuer/is";

import { UNKNOWN_STRING_FAILURE } from "../constants";

import { Errors } from "./errors";

// ***

export type ConfigBase = {
	/** Validation behavior. Defaults to `"assert"` */
	mode: "assert" | "check" | "describe" | "switch";

	/** Strictness of "check" mode. Defaults to `true` */
	checkStrict: boolean;

	/** Error message detalization. Defaults to `"value"` */
	details: "none" | "message" | "value" | "validator" | "validation" | "all";

	/** Show warning if descriptor is defined under multiple names. Defaults to `false` */
	warnOnMultipleNames: boolean;

	/** Whether the smallest and largest values are allowed */
	rangeInclusiveness: [ boolean, boolean ];

	/** Whether the smallest and largest lengths allowed */
	lengthRangeInclusiveness: [ boolean, boolean ];
};

export type Config = Partial<ConfigBase>;

export type Modifier = keyof ConfigBase;
export type ModifierString = Modifier & ("mode" | "details");
export type ModifierNonString = Exclude<Modifier, ModifierString>;

export type ModifierFailure = string;

export type Modification<
	M extends Modifier = Modifier,
> = ConfigBase[M];

// ***

/** @private */
const _isBooleanPair = is.array<boolean>(2, is._boolean);

/** @private */
const NOT_A_BOOLEAN_FAILURE = "is not a boolean";

/** @private */
const NOT_A_PAIR_OF_BOOLEANS_FAILURE = "is not a pair of boolean values";

/** @private */
const MODIFIER_TO_DEFAULT_MODIFICATION_MAP: ConfigBase = {
	mode: "assert",
	checkStrict: true,
	details: "value",
	warnOnMultipleNames: false,
	rangeInclusiveness: [ true, true ],
	lengthRangeInclusiveness: [ true, true ],
};

/** @private */
const MODIFIER_STRING_TO_MODIFICATION_LIST_MAP: {
	[M in ModifierString]: Modification<M>[];
} = {
	mode: [ "assert", "check", "describe", "switch" ],
	details: [ "none", "message", "value", "validator", "validation", "all" ],
};

/** @private */
const MODIFIER_TO_MODIFICATION_VALIDATOR_THUNK_MAP: {
	[M in Modifier]: Thunk<Modification<M>>;
} = {
	...help.convertMapFlat(MODIFIER_STRING_TO_MODIFICATION_LIST_MAP, is.inArray),

	checkStrict: is._boolean,
	warnOnMultipleNames: is._boolean,
	rangeInclusiveness: _isBooleanPair,
	lengthRangeInclusiveness: _isBooleanPair,
};

/** @private */
const MODIFIER_TO_MODIFIER_FAILURE_MAP: {
	[M in Modifier]: ModifierFailure;
} = {
	...help.convertMapFlat(MODIFIER_STRING_TO_MODIFICATION_LIST_MAP, () => UNKNOWN_STRING_FAILURE),

	checkStrict: NOT_A_BOOLEAN_FAILURE,
	warnOnMultipleNames: NOT_A_BOOLEAN_FAILURE,
	rangeInclusiveness: NOT_A_PAIR_OF_BOOLEANS_FAILURE,
	lengthRangeInclusiveness: NOT_A_PAIR_OF_BOOLEANS_FAILURE,
};

/** @private */
const CONFIG_HISTORY: ConfigBase[] = [ MODIFIER_TO_DEFAULT_MODIFICATION_MAP ];

// ***

/** @private */
const isModifier = (modifier: any): modifier is Modifier => {
	return modifier in MODIFIER_TO_DEFAULT_MODIFICATION_MAP;
};

/** @private */
const isModifierString = (modifier: Modifier): modifier is ModifierString => {
	return modifier in MODIFIER_STRING_TO_MODIFICATION_LIST_MAP;
};

// ***

/** @internal */
export const Configs = {
	get current() {
		return CONFIG_HISTORY[CONFIG_HISTORY.length - 1];
	},

	// ***

	validateModifier<M extends Modifier>(modifier: M, modification: Modification<M>) {
		const thunk = MODIFIER_TO_MODIFICATION_VALIDATOR_THUNK_MAP[modifier] as Thunk<Modification<M>>;

		const invalid = () => Errors.panic(
			"Invalid config",
			`"${ modifier }" ${ MODIFIER_TO_MODIFIER_FAILURE_MAP[modifier] }`,
			`modification ${ help.getPrintable(modification) }`,
		);

		if (isModifierString(modifier)) {
			if (!is.inArray(MODIFIER_STRING_TO_MODIFICATION_LIST_MAP[modifier])(modification))
				return invalid();
		}

		else if (!thunk(modification))
			return invalid();
	},

	validate(config: Config): Config {
		const _config: Config = Object(config);

		for (const modifier in _config)
			if (isModifier(modifier))
				Configs.validateModifier(modifier, _config[modifier]);

			else return Errors.unknown.modifier(modifier);

		return _config;
	},

	create(config: Config): ConfigBase {
		return {
			...MODIFIER_TO_DEFAULT_MODIFICATION_MAP,
			...Configs.validate(config),
		};
	},

	clearCurrent() {
		if (CONFIG_HISTORY.length > 1)
			CONFIG_HISTORY.pop();
	},

	setupCurrent(config: Config): ConfigBase {
		Configs.clearCurrent();
		CONFIG_HISTORY.push(Configs.create(config));

		return Configs.current;
	},

	/**
	 * @example
	 * const mode = "check";
	 * const checkStrict = false;
	 *
	 * valuer.config({ mode, checkStrict });
	 */
	changeDefault(config: Config): ConfigBase {
		return Object.assign(MODIFIER_TO_DEFAULT_MODIFICATION_MAP, Configs.validate(config));
	},
};
