import { Dict } from "@valuer/types";
import { help } from "@valuer/help";
import { is } from "@valuer/is";

import { ConfigBase, Role, Failure } from "../types";

import { Configs } from "./configs";
import { Errors, ErrorMessage } from "./errors";
import { Processing, Processors } from "./processors";

// ***

type ValuerError = {
	name: string;
	value: any;
	message: ErrorMessage;
};

export type Result<
	Value = any,
> = Value | boolean | Dict<Failure> | ValuerError;

/** @internal */
export type Hooks<Value = any> = {
	["validation.success"](processing?: Processing): void;
	["validation.failure"](processing?: Processing, role?: Role): void;
	["validation.break"](): boolean;
	["validation.return"](value?: Value): Result<Value>;
};

// ***

/** @private */
function createHooksAssert<Value = any>(): Hooks<Value> {
	return ({
		"validation.success": (processing?) => { /* skip */ },

		"validation.failure": ({ processor, value }, role) =>
			Errors.panic(...Processors.getPanicArgs(processor, value, role)),

		"validation.break": () => false,

		"validation.return": help.transit(),
	});
}

/** @private */
function createHooksCheck<Value = any>(config: ConfigBase): Hooks<Value> {
	const results: boolean[] = [];

	return ({
		"validation.success": (processing?) =>
			results.push(true),

		"validation.failure": (processing?, role?) =>
			results.push(false),

		"validation.break": () =>
			// break if [strict and at least one failure] or [not strict and at least one success]
			results.some(is.boolean(!config.checkStrict)),

		"validation.return": (value?) =>
			// return `true` if either [strict and no failures] or [not strict and at least one success]
			config.checkStrict ? results.every(is.boolean(true)) : results.some(is.boolean(true)),
	});
}

/** @private */
function createHooksDescribe<Value = any>(): Hooks<Value> {
	const describe: Dict<Failure> = {};

	return ({
		"validation.success": ({ processor }) =>
			describe[Processors.getKey(processor)] = null,

		"validation.failure": ({ processor }, role) =>
			describe[Processors.getKey(processor)] = Processors.getMessageTextPart(processor, role), // TODO: #146

		"validation.break": () => false,

		"validation.return": (value?) => describe,
	});
}

/** @private */
function createHooksSwitch<Value = any>(): Hooks<Value> {
	const error: ValuerError = Object.create(null);

	return ({
		"validation.success": (processing?) => { /* skip */ },

		"validation.failure": ({ processor, value }, role?) => {
			error.value = value;
			error.name = Processors.getKey(processor);
			error.message = Processors.getMessage(processor, value, role);
		},

		"validation.break": () => "name" in error,

		"validation.return": (value?) => error,
	});
}

// ***

/** @internal */
export const Hooks = {
	create<Value = any>(config: ConfigBase): Hooks<Value> {
		if (config.mode === "assert")
			return createHooksAssert();

		if (config.mode === "check")
			return createHooksCheck(config);

		if (config.mode === "describe")
			return createHooksDescribe();

		if (config.mode === "switch")
			return createHooksSwitch();

		Configs.validateModifier("mode", config.mode);
	},
};
